from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.urls import reverse
from llenado.forms import FormEstudiante
from llenado.models import Estudiante

# Create your views here.

def formulario_estudiante(request):
   if request.method == 'POST':
       form = FormEstudiante(request.POST)
       if form.is_valid():
           nombre = request.POST.get("nombre")
           miEstudiante = Estudiante.objects.create(nombre=nombre)
           miEstudiante.save()
           return HttpResponse("Estudiante creado con éxito")
   else:
       form = FormEstudiante()
   context = {
       'form': form,
   }
   return render(request, 'datos.html', context)


